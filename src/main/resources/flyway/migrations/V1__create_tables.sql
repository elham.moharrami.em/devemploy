CREATE TABLE developer
(
    id               bigint primary key,
    full_name        varchar(200),
    contact          varchar(200),
    email            varchar(200),
    experience_years integer,
    job_title        varchar(200),
    password         varchar(8)
);

CREATE TABLE employer
(
    id       bigint primary key,
    name     varchar(200),
    contact  varchar(200),
    email    varchar(200),
    password bigint
);

CREATE TABLE project
(
    id           bigint primary key,
    title        varchar(200),
    employer_id  bigint,
    developer_id bigint,
    start_date   bigint,
    end_date     bigint,
    price        numeric(5, 2),
    status       varchar(10)
);

INSERT
INTO developer(id, full_name)
VALUES (1, 'Sam Smith');

INSERT
INTO developer(id, full_name)
VALUES (2, 'Pedro Pascal');


INSERT
INTO developer(id, full_name)
VALUES (3, 'Taylor Swift');


INSERT
INTO developer(id, full_name)
VALUES (4, 'Jason Momoa');

INSERT
INTO developer(id, full_name)
VALUES (5, 'John Cena');


INSERT
INTO employer(id, name)
VALUES (1, 'Microsoft co.');

INSERT
INTO employer(id, name)
VALUES (2, 'Oracle co.');

INSERT
INTO employer(id, name)
VALUES (3, 'Asus co.');

INSERT
INTO employer(id, name)
VALUES (4, 'Sony co.');

INSERT
INTO project(id, title)
VALUES (1, 'tapsi');

INSERT
INTO project(id, title)
VALUES (2, 'digikala');

INSERT
INTO project(id, title)
VALUES (3, 'snapp');

INSERT
INTO project(id, title)
VALUES (4, 'naniva');

INSERT
INTO project(id, title)
VALUES (5, 'bit');