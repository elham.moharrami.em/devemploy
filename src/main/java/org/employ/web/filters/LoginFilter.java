package org.employ.web.filters;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.employ.model.HttpSession;
import org.employ.web.NotAuthorizedException;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Enumeration;

@WebFilter(urlPatterns = {"/*"})
public class LoginFilter implements Filter {
    private static final Logger logger = LogManager.getLogger(LoginFilter.class);

    @Override
    public void init(FilterConfig filterConfig) {
    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        HttpServletRequest request = (HttpServletRequest) servletRequest;
        HttpServletResponse response = (HttpServletResponse) servletResponse;
        Boolean loginExists = checkLoginExists(request);
        if (loginExists) {
            filterChain.doFilter(request, response);
        } else {
            try {
                if (request.getCookies() != null) {
                    for (Cookie cookie : request.getCookies()) {
                        if (cookie.getName().equals("SESSION_ID")) {
                            String sessionId = cookie.getValue();
                            if (!HttpSession.sessions.containsKey(sessionId)) {
                                throw new NotAuthorizedException();
                            } else {
                                filterChain.doFilter(request, response);
                            }
                        }
                    }
                } else {
                    throw new NotAuthorizedException();
                }
            } catch (NotAuthorizedException e) {
                logger.warn("not authenticated");
                response.setStatus(402);
            }
        }
    }

    @Override
    public void destroy() {
    }

    private Boolean checkLoginExists(HttpServletRequest request) {
        Enumeration<String> parameterNames = request.getParameterNames();
        while (parameterNames.hasMoreElements()) {
            String paramName = parameterNames.nextElement();
            String[] paramValues = request.getParameterValues(paramName);
            for (String paramValue : paramValues) {
                if (paramName.equals("method") && paramValue.equals("login")) {
                    return true;
                }
            }
        }
        return false;
    }
}
