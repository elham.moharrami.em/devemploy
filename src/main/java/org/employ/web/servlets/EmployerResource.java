package org.employ.web.servlets;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.employ.model.Employer;
import org.employ.service.EmployerService;
import org.employ.service.implementations.EmployerServiceImpl;
import org.employ.web.BaseWeb;
import org.employ.web.SecurityService;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import java.util.Optional;

public class EmployerResource extends HttpServlet {
    private static final Logger logger = LogManager.getLogger(EmployerResource.class);
    private final EmployerService employerService = new EmployerServiceImpl();
    private final BaseWeb baseWeb = new BaseWeb();
    private final SecurityService login = new SecurityService();

    public void doGet(HttpServletRequest request, HttpServletResponse response) {
        String method = request.getParameter("method");
        switch (method) {
            case "findById":
                    Long id = Long.valueOf(request.getParameter("id"));
                    findById(response, id);
                break;
            case "findAll":
                    findAll(response);
                break;
            case "login":
                login.login(request, response);
                break;
        }
    }

    public void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException {
            String body = baseWeb.getBody(request);
            ObjectMapper objectMapper = new ObjectMapper();
            Employer employer = objectMapper.readValue(body, Employer.class);
            employerService.addEmployer(employer);
    }

    public void doDelete(HttpServletRequest request, HttpServletResponse response) {
            Long id = Long.valueOf(request.getParameter("id"));
            employerService.deleteById(id);
    }

    private void findAll(HttpServletResponse response) {
        try {
            List<Employer> allEmployers = employerService.getAllEmployers();
            ObjectMapper objectMapper = new ObjectMapper();
            objectMapper.disable(SerializationFeature.FAIL_ON_EMPTY_BEANS);
            PrintWriter out = response.getWriter();
            response.setContentType("application/json");
            response.setCharacterEncoding("UTF-8");
            out.print(objectMapper.writeValueAsString(allEmployers));
            out.flush();
        } catch (IOException e) {
            logger.warn("could not get all employers." + e.getMessage());
        }
    }

    private void findById(HttpServletResponse response, Long id) {
        try {
            Optional<Employer> employer = employerService.getEmployer(id);
            ObjectMapper objectMapper = new ObjectMapper();
            objectMapper.disable(SerializationFeature.FAIL_ON_EMPTY_BEANS);
            PrintWriter out = response.getWriter();
            response.setContentType("application/json");
            response.setCharacterEncoding("UTF-8");
            out.print(objectMapper.writeValueAsString(employer.get()));
            out.flush();
        } catch (IOException e) {
            logger.warn("could not get developer." + e.getMessage());
        }
    }

}
