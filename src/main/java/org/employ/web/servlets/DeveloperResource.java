package org.employ.web.servlets;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.employ.model.Developer;
import org.employ.service.DeveloperService;
import org.employ.service.implementations.DeveloperServiceImpl;
import org.employ.web.BaseWeb;
import org.employ.web.SecurityService;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import java.util.Optional;

public class DeveloperResource extends HttpServlet {
    private static final Logger logger = LogManager.getLogger(DeveloperResource.class);
    private final DeveloperService developerService = new DeveloperServiceImpl();
    private final BaseWeb baseWeb = new BaseWeb();
    private final SecurityService login = new SecurityService();

    public void doGet(HttpServletRequest request, HttpServletResponse response) {
        String method = request.getParameter("method");
        switch (method) {
            case "findById":
                    Long id = Long.valueOf(request.getParameter("id"));
                    findById(response, id);
                break;
            case "findAll":
                    findAll(response);
                break;
            case "login":
                login.login(request, response);
                break;
        }
    }

    public void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException {
            String body = baseWeb.getBody(request);
            ObjectMapper objectMapper = new ObjectMapper();
            Developer developer = objectMapper.readValue(body, Developer.class);
            developerService.addDeveloper(developer);
    }

    public void doDelete(HttpServletRequest request, HttpServletResponse response) {
            Long id = Long.valueOf(request.getParameter("id"));
            developerService.deleteById(id);
    }

    private void findAll(HttpServletResponse response) {
        try {
            List<Developer> allDevelopers = developerService.getAllDevelopers();
            ObjectMapper objectMapper = new ObjectMapper();
            objectMapper.disable(SerializationFeature.FAIL_ON_EMPTY_BEANS);
            PrintWriter out = response.getWriter();
            response.setContentType("application/json");
            response.setCharacterEncoding("UTF-8");
            out.print(objectMapper.writeValueAsString(allDevelopers));
            out.flush();
        } catch (IOException e) {
            logger.warn("could not get all developers." + e.getMessage());
        }
    }

    private void findById(HttpServletResponse response, Long id) {
        try {
            Optional<Developer> developer = developerService.getDeveloper(id);
            ObjectMapper objectMapper = new ObjectMapper();
            objectMapper.disable(SerializationFeature.FAIL_ON_EMPTY_BEANS);
            PrintWriter out = response.getWriter();
            response.setContentType("application/json");
            response.setCharacterEncoding("UTF-8");
            out.print(objectMapper.writeValueAsString(developer.get()));
            out.flush();
        } catch (IOException e) {
            logger.warn("could not get developer." + e.getMessage());
        }
    }

}
