package org.employ.web.servlets;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.employ.model.Project;
import org.employ.service.ProjectService;
import org.employ.service.implementations.ProjectServiceImpl;
import org.employ.web.BaseWeb;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import java.util.Optional;

public class ProjectResource extends HttpServlet {
    private static final Logger logger = LogManager.getLogger(ProjectResource.class);
    private final ProjectService projectService = new ProjectServiceImpl();
    private final BaseWeb baseWeb = new BaseWeb();

    public void doGet(HttpServletRequest request, HttpServletResponse response) {
        String method = request.getParameter("method");
            switch (method) {
                case "findById":
                    Long id = Long.valueOf(request.getParameter("id"));
                    findById(response, id);
                    break;
                case "findAll":
                    findAll(response);
                    break;
            }
    }

    public void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException {
            String body = baseWeb.getBody(request);
            ObjectMapper objectMapper = new ObjectMapper();
            Project project = objectMapper.readValue(body, Project.class);
            System.out.println(project.toString());
            projectService.addProject(project);
    }

    public void doDelete(HttpServletRequest request, HttpServletResponse response) {
            Long id = Long.valueOf(request.getParameter("id"));
            projectService.deleteById(id);
    }

    private void findAll(HttpServletResponse response) {
        try {
            List<Project> allProjects = projectService.getAllProjects();
            ObjectMapper objectMapper = new ObjectMapper();
            objectMapper.disable(SerializationFeature.FAIL_ON_EMPTY_BEANS);
            PrintWriter out = response.getWriter();
            response.setContentType("application/json");
            response.setCharacterEncoding("UTF-8");
            out.print(objectMapper.writeValueAsString(allProjects));
            out.flush();
        } catch (IOException e) {
            logger.warn("could not get all projects." + e.getMessage());
        }
    }

    private void findById(HttpServletResponse response, Long id) {
        try {
            Optional<Project> project = projectService.getProjectById(id);
            ObjectMapper objectMapper = new ObjectMapper();
            objectMapper.disable(SerializationFeature.FAIL_ON_EMPTY_BEANS);
            PrintWriter out = response.getWriter();
            response.setContentType("application/json");
            response.setCharacterEncoding("UTF-8");
            out.print(objectMapper.writeValueAsString(project.get()));
            out.flush();
        } catch (IOException e) {
            logger.warn("could not get project." + e.getMessage());
        }
    }
}
