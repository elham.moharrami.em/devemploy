package org.employ.web;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.employ.model.HttpSession;
import org.employ.service.DeveloperService;
import org.employ.service.EmployerService;
import org.employ.service.implementations.DeveloperServiceImpl;
import org.employ.service.implementations.EmployerServiceImpl;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Optional;
import java.util.UUID;

public class SecurityService {
    private final DeveloperService developerService = new DeveloperServiceImpl();
    private final EmployerService employerService = new EmployerServiceImpl();
    private static final Logger logger = LogManager.getLogger(SecurityService.class);

    public void login(HttpServletRequest request, HttpServletResponse response) {
        try {
            String email = request.getParameter("email");
            String password = request.getParameter("password");
            String role = request.getParameter("role");
            PrintWriter out = response.getWriter();
            ObjectMapper objectMapper = new ObjectMapper();
            objectMapper.disable(SerializationFeature.FAIL_ON_EMPTY_BEANS);
            UUID uuid = UUID.randomUUID();
            Optional<?> object = Optional.empty();
            if (role.equals("developer")) {
                object = developerService.loginDeveloper(email, password);
            } else if (role.equals("employer")) {
                object = employerService.loginEmployer(email, password);
            }
            if (object.isPresent()) {
                HttpSession.sessions.put(uuid.toString(), object.get());
                response.setContentType("application/json");
                response.setCharacterEncoding("UTF-8");
                response.addCookie(new Cookie("SESSION_ID", uuid.toString()));
                out.print(objectMapper.writeValueAsString(object.get()));
                out.flush();
            } else {
                throw new UsernameOrPasswordIsNotCorrectException();
            }
        } catch (IOException e) {
            logger.warn("could not login." + e.getMessage());
        } catch (UsernameOrPasswordIsNotCorrectException e) {
            response.setStatus(402);
        }
    }
}