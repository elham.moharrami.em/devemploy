package org.employ.service;

import org.employ.model.Developer;
import org.employ.model.Employer;

import java.util.List;
import java.util.Optional;

public interface EmployerService {
    Optional<Employer> getEmployer(Long id);

    void addEmployer(Employer employer);

    List<Employer> getAllEmployers();

    void deleteById(Long id);

    Optional<Employer>  loginEmployer(String email, String password);
}
