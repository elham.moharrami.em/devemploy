package org.employ.service;

import org.employ.model.Project;

import java.util.List;
import java.util.Optional;

public interface ProjectService {
    Optional<Project> getProjectById(Long id);

    List<Project> getAllProjects();

    void addProject(Project project);

    void deleteById(Long id);

    void deleteByDeveloperId(Long id);

    void deleteByEmployerId(Long id);
}
