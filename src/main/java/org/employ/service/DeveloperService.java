package org.employ.service;

import org.employ.model.Developer;

import java.util.List;
import java.util.Optional;

public interface DeveloperService {
    Optional<Developer> getDeveloper(Long id);

    void addDeveloper(Developer developer);

    List<Developer> getAllDevelopers();

    void deleteById(Long id);

    Optional<Developer> loginDeveloper(String email, String password);
}
