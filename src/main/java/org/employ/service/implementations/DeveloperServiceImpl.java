package org.employ.service.implementations;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.employ.model.Developer;
import org.employ.repository.DeveloperRepository;
import org.employ.repository.jdbc.DeveloperRepositoryJdbcImpl;
import org.employ.service.DeveloperService;
import org.employ.service.ProjectService;

import java.sql.SQLException;
import java.util.List;
import java.util.Optional;

public class DeveloperServiceImpl implements DeveloperService {
    private final DeveloperRepository developerRepository = new DeveloperRepositoryJdbcImpl();
    private static final Logger logger = LogManager.getLogger(DeveloperServiceImpl.class);

    private final ProjectService projectService = new ProjectServiceImpl();

    @Override
    public Optional<Developer> getDeveloper(Long id) {
        Optional<Developer> developer = developerRepository.findById(id);
        if (developer.isPresent()) {
            return developer;
        } else {
            logger.info("there is no developer with this id present");
        }
        return Optional.empty();
    }

    @Override
    public void addDeveloper(Developer developer) {
       try{
           developerRepository.add(developer);
       } catch (SQLException e) {
           logger.info("could not add developer."+e.getMessage());
       }
    }

    @Override
    public List<Developer> getAllDevelopers() {
        return developerRepository.getAll();
    }

    @Override
    public void deleteById(Long id) {
        try {
            projectService.deleteByDeveloperId(id);
            developerRepository.deleteById(id);
        } catch (SQLException | ClassNotFoundException e) {
            logger.warn("failed at attempting to delete developer and projects involved.");
        }
    }

    @Override
    public Optional<Developer> loginDeveloper(String email, String password) {
        return developerRepository.login(email, password);
    }
}
