package org.employ.service.implementations;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.employ.model.Employer;
import org.employ.repository.EmployerRepository;
import org.employ.repository.jdbc.EmployerRepositoryJdbcImpl;
import org.employ.service.EmployerService;
import org.employ.service.ProjectService;

import java.sql.SQLException;
import java.util.List;
import java.util.Optional;

public class EmployerServiceImpl implements EmployerService {
    private static final Logger logger = LogManager.getLogger(EmployerServiceImpl.class);
    private final EmployerRepository employerRepository = new EmployerRepositoryJdbcImpl();
    private final ProjectService projectService = new ProjectServiceImpl();

    @Override
    public Optional<Employer> getEmployer(Long id) {
        Optional<Employer> employer = employerRepository.findById(id);
        if (employer.isPresent()) {
            return employer;
        } else {
            logger.info("there is no employer with this id present");
        }
        return Optional.empty();
    }

    @Override
    public void addEmployer(Employer employer) {
        try {
            employerRepository.add(employer);
        } catch (SQLException e) {
            logger.info("could not add employer." + e.getMessage());
        }
    }

    @Override
    public List<Employer> getAllEmployers() {
        return employerRepository.getAll();
    }

    @Override
    public void deleteById(Long id) {
        try {
            projectService.deleteByEmployerId(id);
            employerRepository.deleteById(id);
        } catch (SQLException | ClassNotFoundException e) {
            logger.warn("failed at attempting to delete employer and projects involved.");
        }
    }

    @Override
    public Optional<Employer> loginEmployer(String email, String password) {
        return employerRepository.login(email, password);
    }
}
