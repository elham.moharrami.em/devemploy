package org.employ.service.implementations;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.employ.model.Project;
import org.employ.repository.ProjectRepository;
import org.employ.repository.jdbc.ProjectRepositoryJdbcImpl;
import org.employ.service.ProjectService;

import java.sql.SQLException;
import java.util.List;
import java.util.Optional;

public class ProjectServiceImpl implements ProjectService {
    private static final Logger logger = LogManager.getLogger(ProjectServiceImpl.class);
    private final ProjectRepository projectRepository = new ProjectRepositoryJdbcImpl();


    @Override
    public Optional<Project> getProjectById(Long id) {
        Optional<Project> project = projectRepository.findById(id);
        if (project.isPresent()) {
            return project;
        } else {
            logger.info("there is no project with this id present");
        }
        return Optional.empty();
    }

    @Override
    public List<Project> getAllProjects() {
        return projectRepository.getAll();
    }

    @Override
    public void addProject(Project project) {
        try {
            projectRepository.add(project);
        } catch (SQLException e) {
            logger.info("could not add project." + e.getMessage());
        }
    }

    @Override
    public void deleteById(Long id) {
        try {
            projectRepository.deleteById(id);
        } catch (SQLException | ClassNotFoundException e) {
            logger.warn("failed to delete project by id." + e.getMessage());
        }
    }

    @Override
    public void deleteByDeveloperId(Long id) {
        try {
            projectRepository.deleteByDeveloperId(id);
        } catch (SQLException e) {
            logger.warn("failed to delete project via developer id." + e.getMessage());
        }
    }

    @Override
    public void deleteByEmployerId(Long id) {
        try {
            projectRepository.deleteByEmployerId(id);
        } catch (SQLException e) {
            logger.warn("failed to delete project via employer id." + e.getMessage());
        }
    }
}
