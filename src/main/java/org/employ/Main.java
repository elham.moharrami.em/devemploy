package org.employ;

import org.employ.database.config.DatabaseCreator;

public class Main {
    public static void main(String[] args) {
        DatabaseCreator databaseCreator = new DatabaseCreator();
        databaseCreator.createDatabase();
    }
}