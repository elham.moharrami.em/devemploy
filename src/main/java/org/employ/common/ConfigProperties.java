package org.employ.common;

import lombok.Getter;
import lombok.experimental.Accessors;

@Getter
@Accessors(fluent=true)
public class ConfigProperties {
    private final String rawDbUrl;
    private final String dbUrl;
    private final String dbUser;
    private final String dbPassword;
    private final String dbName;

    public ConfigProperties() {
        ConfigLoader configLoader = new ConfigLoader();
        this.rawDbUrl = configLoader.loadConfig("database.raw.url");
        this.dbUrl = configLoader.loadConfig("database.url");
        this.dbUser = configLoader.loadConfig("database.user");
        this.dbPassword = configLoader.loadConfig("database.password");
        this.dbName = configLoader.loadConfig("database.name");
    }
}
