package org.employ.common;

import lombok.Cleanup;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class ConfigLoader {
    private static final Logger logger = LogManager.getLogger(ConfigLoader.class);
    private final Properties properties = new Properties();

    public ConfigLoader() {
        try {
            @Cleanup InputStream propsInput = getClass().getClassLoader().getResourceAsStream("application.properties");

            properties.load(propsInput);
        } catch (IOException e) {
            logger.error("couldn't create configLoader");
        }
    }

    public String loadConfig(String property) {
        try {
            return properties.getProperty(property);
        } catch (Exception e) {
            logger.error("something went wrong");
        }
        return null;
    }
}
