package org.employ.model;

import lombok.*;

@RequiredArgsConstructor
@NoArgsConstructor(force = true)
@ToString(exclude = {"password"})
@Getter
@Setter
public class Account {
    private final @NonNull Long id;
    private final String name;
    private final String email;
    private final Long password;
    private final String contact;
}
