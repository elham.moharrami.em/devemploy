package org.employ.model;

import lombok.*;

import java.math.BigDecimal;
import java.util.Optional;

@RequiredArgsConstructor
@NoArgsConstructor(force = true)
@Getter
@Setter
@ToString(callSuper = true)
public class Project {
    private @NonNull Long id;
    private final String title;
    private final Long startDate;
    private final Long endDate;
    private final ProjectStatus projectStatus;
    private final Optional<Developer> developer;
    private final Optional<Employer> employer;
    private final BigDecimal price;
}

