package org.employ.model;

import lombok.*;

import java.util.List;

@ToString(callSuper = true)
@Getter
@Setter
@NoArgsConstructor(force = true)
public class Employer extends Account {
    private List<Project> projects;

    public Employer(@NonNull Long id, String name, String email, Long password, String contact) {
        super(id, name, email, password, contact);
    }
}
