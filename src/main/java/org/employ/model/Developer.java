package org.employ.model;

import lombok.*;

import java.util.List;

@ToString(callSuper = true)
@Getter
@Setter
@NoArgsConstructor(force = true)
public class Developer extends Account {
    private final String jobTitle;
    private final Integer experience;
    private List<Project> projects;

    public Developer(@NonNull Long id, String name, String email, Long password, String contact, String jobTitle, Integer experience) {
        super(id, name, email, password, contact);
        this.jobTitle = jobTitle;
        this.experience = experience;
    }

}
