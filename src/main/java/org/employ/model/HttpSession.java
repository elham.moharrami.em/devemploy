package org.employ.model;

import lombok.Getter;
import lombok.Setter;

import java.util.HashMap;

@Setter
@Getter
public class HttpSession {
    public static HashMap<String, Object> sessions = new HashMap<>();
}
