package org.employ.repository.jdbc;

import lombok.Cleanup;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.employ.common.ConfigProperties;
import org.employ.model.Employer;
import org.employ.repository.EmployerRepository;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class EmployerRepositoryJdbcImpl implements EmployerRepository {
    private static final Logger logger = LogManager.getLogger(EmployerRepositoryJdbcImpl.class);
    private final ConfigProperties configProperties = new ConfigProperties();
    private static final String FIND_BY_ID_QUERY = "SELECT * FROM employer WHERE id=?";
    private static final String GET_ALL_QUERY = "SELECT * FROM employer";
    private static final String DELETE_BY_ID_QUERY = "DELETE FROM employer WHERE id=?";
    private static final String ADD_QUERY = "INSERT INTO employer VALUES(?,?,?,?,?)";
    private static final String FIND_BY_EMAIL_PASSWORD = "SELECT * FROM employer WHERE email=? AND password=?";

    private List<Employer> constructEmployer(ResultSet resultSet) {
        try {
            List<Employer> employerList = new ArrayList<>();
            while (resultSet.next()) {
                Employer employer;
                Long employerId = resultSet.getLong("id");
                String employerName = resultSet.getString("name");
                String employerEmail = resultSet.getString("email");
                String employerContact = resultSet.getString("contact");
                Long employerPassword = resultSet.getLong("password");
                employer = new Employer(employerId, employerName, employerEmail, employerPassword,
                        employerContact);
                employerList.add(employer);
            }
            return employerList;
        } catch (SQLException e) {
            logger.warn("couldn't create employer." + e.getMessage());
        }
        return null;
    }

    @Override
    public Optional<Employer> findById(Long id) {
        try {
            Class.forName("org.postgresql.Driver");
            @Cleanup Connection connection = DriverManager.getConnection(configProperties.dbUrl(),
                    configProperties.dbUser(), configProperties.dbPassword());
            @Cleanup PreparedStatement preparedStatement = connection.prepareStatement(FIND_BY_ID_QUERY);
            preparedStatement.setLong(1, id);
            ResultSet resultSet = preparedStatement.executeQuery();
            List<Employer> list = constructEmployer(resultSet);
            for (Employer developer : list) {
                return Optional.ofNullable(developer);
            }
        } catch (SQLException | ClassNotFoundException e) {
            logger.warn("couldn't execute find by id query." + e.getMessage());
        }
        return Optional.empty();
    }

    @Override
    public void deleteById(Long id) throws SQLException {
        @Cleanup Connection connection = null;
        try {
            Class.forName("org.postgresql.Driver");
            connection = DriverManager.getConnection(configProperties.dbUrl(),
                    configProperties.dbUser(), configProperties.dbPassword());
            @Cleanup PreparedStatement preparedStatement = connection.prepareStatement(DELETE_BY_ID_QUERY);
            connection.setAutoCommit(false);
            preparedStatement.setLong(1, id);
            int deleted = preparedStatement.executeUpdate();
            connection.commit();
            connection.setAutoCommit(true);
            logger.info(deleted + " row deleted from employer table.");
        } catch (SQLException | ClassNotFoundException e) {
            logger.warn("couldn't delete employer with id " + id + e.getMessage());
            assert connection != null;
            connection.rollback();
        }
    }

    @Override
    public List<Employer> getAll() {
        try {
            Class.forName("org.postgresql.Driver");
            @Cleanup Connection connection = DriverManager.getConnection(configProperties.dbUrl(),
                    configProperties.dbUser(), configProperties.dbPassword());
            @Cleanup Statement statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery(GET_ALL_QUERY);
            return constructEmployer(resultSet);
        } catch (SQLException | ClassNotFoundException e) {
            logger.warn("couldn't get all employers list." + e.getMessage());
        }
        return null;
    }

    @Override
    public void add(Employer object) throws SQLException {
        @Cleanup Connection connection = null;
        try {
            Class.forName("org.postgresql.Driver");
            connection = DriverManager.getConnection(configProperties.dbUrl(),
                    configProperties.dbUser(), configProperties.dbPassword());
            @Cleanup PreparedStatement preparedStatement = connection.prepareStatement(ADD_QUERY);
            connection.setAutoCommit(false);
            preparedStatement.setLong(1, object.getId());
            preparedStatement.setString(2, object.getName());
            preparedStatement.setString(3, object.getContact());
            preparedStatement.setString(4, object.getEmail());
            preparedStatement.setLong(5, object.getPassword() == null ? 0L : object.getPassword());
            int addedRow = preparedStatement.executeUpdate();
            connection.commit();
            connection.setAutoCommit(true);
            logger.info(addedRow + " rows added to employer table.");
        } catch (SQLException | ClassNotFoundException e) {
            logger.warn("couldn't execute add employer query." + e.getMessage());
            assert connection != null;
            connection.rollback();
        }
    }

    @Override
    public Optional<Employer> login(String email, String password) {
        try {
            Class.forName("org.postgresql.Driver");
            @Cleanup Connection connection = DriverManager.getConnection(configProperties.dbUrl(),
                    configProperties.dbUser(), configProperties.dbPassword());
            @Cleanup PreparedStatement preparedStatement = connection.prepareStatement(FIND_BY_EMAIL_PASSWORD);
            preparedStatement.setString(1, email);
            preparedStatement.setString(2, password);
            ResultSet resultSet = preparedStatement.executeQuery();
            List<Employer> list = constructEmployer(resultSet);
            for (Employer employer : list) {
                return Optional.ofNullable(employer);
            }
        } catch (SQLException | ClassNotFoundException e) {
            logger.warn("couldn't execute find by id query." + e.getMessage());
        }
        return Optional.empty();
    }
}