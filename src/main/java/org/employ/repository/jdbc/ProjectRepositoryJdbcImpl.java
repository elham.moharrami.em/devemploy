package org.employ.repository.jdbc;

import lombok.Cleanup;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.employ.common.ConfigProperties;
import org.employ.model.Developer;
import org.employ.model.Employer;
import org.employ.model.Project;
import org.employ.model.ProjectStatus;
import org.employ.repository.DeveloperRepository;
import org.employ.repository.EmployerRepository;
import org.employ.repository.ProjectRepository;

import java.math.BigDecimal;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class ProjectRepositoryJdbcImpl implements ProjectRepository {
    private static final Logger logger = LogManager.getLogger(ProjectRepositoryJdbcImpl.class);
    private final ConfigProperties configProperties = new ConfigProperties();
    private final DeveloperRepository developerRepository = new DeveloperRepositoryJdbcImpl();
    private final EmployerRepository employerRepository = new EmployerRepositoryJdbcImpl();
    private static final String FIND_BY_ID_QUERY = "SELECT * FROM project WHERE id=?";
    private static final String GET_ALL_QUERY = "SELECT * FROM project";
    private static final String DELETE_BY_ID_QUERY = "DELETE FROM project WHERE id=?";
    private static final String DELETE_BY_Developer_ID_QUERY = "DELETE FROM project WHERE developer_id=?";
    private static final String DELETE_BY_Employer_ID_QUERY = "DELETE FROM project WHERE employer_id=?";
    private static final String ADD_QUERY = "INSERT INTO project VALUES(?,?,?,?,?,?,?,?)";

    private List<Project> constructProject(ResultSet resultSet) {
        try {
            List<Project> projectList = new ArrayList<>();
            while (resultSet.next()) {
                Project project;
                Long projectId = resultSet.getLong("id");
                String projectTitle = resultSet.getString("title");
                ProjectStatus projectStatus = resultSet.getString("status") == null ?
                        ProjectStatus.NOSTATUS : ProjectStatus.valueOf(resultSet.getString("status").toUpperCase());
                Long developerId = resultSet.getLong("developer_id");
                Optional<Developer> developer = developerRepository.findById(developerId);
                Long employerId = resultSet.getLong("employer_id");
                Optional<Employer> employer = employerRepository.findById(employerId);
                Long startDate = resultSet.getLong("start_date");
                Long endDate = resultSet.getLong("end_date");
                BigDecimal price = BigDecimal.valueOf(resultSet.getDouble("price"));
                project = new Project(projectId, projectTitle, startDate, endDate, projectStatus,
                        developer, employer, price);
                projectList.add(project);
            }
            return projectList;
        } catch (SQLException e) {
            logger.warn("couldn't create project." + e.getMessage());
        }
        return null;
    }

    @Override
    public Optional<Project> findById(Long id) {
        try {
            Class.forName("org.postgresql.Driver");
            @Cleanup Connection connection = DriverManager.getConnection(configProperties.dbUrl(),
                    configProperties.dbUser(), configProperties.dbPassword());
            @Cleanup PreparedStatement preparedStatement = connection.prepareStatement(FIND_BY_ID_QUERY);
            preparedStatement.setLong(1, id);
            ResultSet resultSet = preparedStatement.executeQuery();
            List<Project> list = constructProject(resultSet);
            for (Project project : list) {
                return Optional.ofNullable(project);
            }
        } catch (SQLException | ClassNotFoundException e) {
            logger.warn("couldn't execute find by id query." + e.getMessage());
        }
        return Optional.empty();
    }

    @Override

    public void deleteById(Long id) throws SQLException, ClassNotFoundException {
        @Cleanup Connection connection = null;
        try {
            Class.forName("org.postgresql.Driver");
            connection = DriverManager.getConnection(configProperties.dbUrl(),
                    configProperties.dbUser(), configProperties.dbPassword());
            @Cleanup PreparedStatement preparedStatement = connection.prepareStatement(DELETE_BY_ID_QUERY);
            connection.setAutoCommit(false);
            preparedStatement.setLong(1, id);
            int deleted = preparedStatement.executeUpdate();
            connection.commit();
            connection.setAutoCommit(true);
            logger.info(deleted + " row deleted from project table.");
        } catch (SQLException e) {
            logger.warn("couldn't delete project with id " + id + e.getMessage());
            assert connection != null;
            connection.rollback();
        }
    }

    @Override
    public List<Project> getAll() {
        try {
            Class.forName("org.postgresql.Driver");
            @Cleanup Connection connection = DriverManager.getConnection(configProperties.dbUrl(),
                    configProperties.dbUser(), configProperties.dbPassword());
            @Cleanup Statement statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery(GET_ALL_QUERY);
            return constructProject(resultSet);
        } catch (SQLException | ClassNotFoundException e) {
            logger.warn("couldn't get all projects list." + e.getMessage());
        }
        return null;
    }

    @Override
    public void add(Project object) throws SQLException {
        @Cleanup Connection connection = null;
        try {
            Class.forName("org.postgresql.Driver");
            connection = DriverManager.getConnection(configProperties.dbUrl(),
                    configProperties.dbUser(), configProperties.dbPassword());
            @Cleanup PreparedStatement preparedStatement = connection.prepareStatement(ADD_QUERY);
            connection.setAutoCommit(false);
            preparedStatement.setLong(1, object.getId());
            preparedStatement.setString(2, object.getTitle());
            preparedStatement.setLong(3, object.getEmployer() == null ? 0L : object.getEmployer().get().getId());
            preparedStatement.setLong(4, object.getDeveloper() == null ? 0L : object.getDeveloper().get().getId());
            preparedStatement.setLong(5, object.getStartDate() == null ? 0L : object.getStartDate());
            preparedStatement.setLong(6, object.getEndDate() == null ? 0L : object.getEndDate());
            preparedStatement.setBigDecimal(7, object.getPrice());
            preparedStatement.setString(8, String.valueOf(object.getProjectStatus()));
            int addedRow = preparedStatement.executeUpdate();
            connection.commit();
            connection.setAutoCommit(true);
            logger.info(addedRow + " rows added to project table.");
        } catch (SQLException | ClassNotFoundException e) {
            logger.warn("couldn't execute add project query." + e.getMessage());
            assert connection != null;
            connection.rollback();
        }
    }

    @Override
    public void deleteByDeveloperId(Long id) throws SQLException {
        @Cleanup Connection connection = null;
        try {
            Class.forName("org.postgresql.Driver");
            connection = DriverManager.getConnection(configProperties.dbUrl(),
                    configProperties.dbUser(), configProperties.dbPassword());
            @Cleanup PreparedStatement preparedStatement = connection.prepareStatement(DELETE_BY_Developer_ID_QUERY);
            connection.setAutoCommit(false);
            preparedStatement.setLong(1, id);
            int deleted = preparedStatement.executeUpdate();
            connection.commit();
            connection.setAutoCommit(true);
            logger.info(deleted + " row deleted from project table.");
        } catch (SQLException | ClassNotFoundException e) {
            logger.warn("couldn't delete project with id " + id + e.getMessage());
            assert connection != null;
            connection.rollback();
        }
    }

    @Override
    public void deleteByEmployerId(Long id) throws SQLException {
        @Cleanup Connection connection = null;
        try {
            Class.forName("org.postgresql.Driver");
            connection = DriverManager.getConnection(configProperties.dbUrl(),
                    configProperties.dbUser(), configProperties.dbPassword());
            @Cleanup PreparedStatement preparedStatement = connection.prepareStatement(DELETE_BY_Employer_ID_QUERY);
            connection.setAutoCommit(false);
            preparedStatement.setLong(1, id);
            int deleted = preparedStatement.executeUpdate();
            connection.commit();
            connection.setAutoCommit(true);
            logger.info(deleted + " row deleted from project table.");
        } catch (SQLException | ClassNotFoundException e) {
            logger.warn("couldn't delete project with id " + id + e.getMessage());
            connection.rollback();
        }
    }
}
