package org.employ.repository.jdbc;

import lombok.Cleanup;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.employ.common.ConfigProperties;
import org.employ.model.Developer;
import org.employ.repository.DeveloperRepository;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class DeveloperRepositoryJdbcImpl implements DeveloperRepository {
    private static final Logger logger = LogManager.getLogger(DeveloperRepositoryJdbcImpl.class);
    private final ConfigProperties configProperties = new ConfigProperties();
    private static final String FIND_BY_ID_QUERY = "SELECT * FROM developer WHERE id=?";
    private static final String GET_ALL_QUERY = "SELECT * FROM developer";
    private static final String DELETE_BY_ID_QUERY = "DELETE FROM developer WHERE id=?";
    private static final String ADD_QUERY = "INSERT INTO developer VALUES(?,?,?,?,?,?,?,?)";
    private static final String FIND_BY_EMAIL_PASSWORD = "SELECT * FROM developer WHERE email=? AND password=?";

    private List<Developer> constructDeveloper(ResultSet resultSet) throws SQLException {
        try {
            List<Developer> developerList = new ArrayList<>();
            while (resultSet.next()) {
                Developer developer;
                Long developerId = resultSet.getLong("id");
                String developerName = resultSet.getString("full_name");
                String developerEmail = resultSet.getString("email");
                Long developerPassword = resultSet.getLong("password");
                String developerContact = resultSet.getString("contact");
                String developerJobTitle = resultSet.getString("job_title");
                Integer developerExperience = null;
                if (resultSet.getString("experience_years") != null) {
                    developerExperience = Integer.valueOf(resultSet.getString("experience_years"));
                }
                developer = new Developer(developerId, developerName, developerEmail, developerPassword,
                        developerContact, developerJobTitle, developerExperience);
                developerList.add(developer);
            }
            return developerList;
        } catch (SQLException e) {
            logger.warn("couldn't create developer." + e.getMessage());
        }
        return null;
    }

    @Override
    public Optional<Developer> findById(Long id) {
        try {
            Class.forName("org.postgresql.Driver");
            @Cleanup Connection connection = DriverManager.getConnection(configProperties.dbUrl(),
                    configProperties.dbUser(), configProperties.dbPassword());
            @Cleanup PreparedStatement preparedStatement = connection.prepareStatement(FIND_BY_ID_QUERY);
            preparedStatement.setLong(1, id);
            ResultSet resultSet = preparedStatement.executeQuery();
            List<Developer> list = constructDeveloper(resultSet);
            for (Developer developer : list) {
                return Optional.ofNullable(developer);
            }
        } catch (SQLException | ClassNotFoundException e) {
            logger.warn("couldn't execute find by id query." + e.getMessage());
        }
        return Optional.empty();
    }

    @Override
    public void deleteById(Long id) throws SQLException {
        @Cleanup Connection connection = null;
        try {
            Class.forName("org.postgresql.Driver");
            connection = DriverManager.getConnection(configProperties.dbUrl(),
                    configProperties.dbUser(), configProperties.dbPassword());
            @Cleanup PreparedStatement preparedStatement = connection.prepareStatement(DELETE_BY_ID_QUERY);
            connection.setAutoCommit(false);
            preparedStatement.setLong(1, id);
            int deleted = preparedStatement.executeUpdate();
            connection.commit();
            logger.info(deleted + " row deleted from developer table.");
            connection.setAutoCommit(true);
        } catch (SQLException | ClassNotFoundException e) {
            logger.warn("couldn't delete developer with id " + id + e.getMessage());
            assert connection != null;
            connection.rollback();
        }
    }

    @Override
    public List<Developer> getAll() {
        try {
            Class.forName("org.postgresql.Driver");
            @Cleanup Connection connection = DriverManager.getConnection(configProperties.dbUrl(),
                    configProperties.dbUser(), configProperties.dbPassword());
            @Cleanup Statement statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery(GET_ALL_QUERY);
            return constructDeveloper(resultSet);
        } catch (SQLException | ClassNotFoundException e) {
            logger.warn("couldn't get all developers list." + e.getMessage());
        }
        return null;
    }

    @Override
    public void add(Developer object) throws SQLException {
        @Cleanup Connection connection = null;
        try {
            Class.forName("org.postgresql.Driver");
            connection = DriverManager.getConnection(configProperties.dbUrl(),
                    configProperties.dbUser(), configProperties.dbPassword());
            @Cleanup PreparedStatement preparedStatement = connection.prepareStatement(ADD_QUERY);
            connection.setAutoCommit(false);
            preparedStatement.setLong(1, object.getId());
            preparedStatement.setString(2, object.getName());
            preparedStatement.setString(3, object.getContact());
            preparedStatement.setString(4, object.getEmail());
            preparedStatement.setLong(5, object.getExperience() == null ? 0L : object.getExperience());
            preparedStatement.setString(6, object.getJobTitle());
            preparedStatement.setLong(7, object.getPassword() == null ? 0L : object.getPassword());
            int addedRow = preparedStatement.executeUpdate();
            connection.commit();
            connection.setAutoCommit(true);
            logger.info(addedRow + " rows added to developer table.");
        } catch (SQLException | ClassNotFoundException e) {
            logger.warn("couldn't execute add developer query." + e.getMessage());
            assert connection != null;
            connection.rollback();
        }
    }


    @Override
    public Optional<Developer> login(String email, String password) {
        try {
            Class.forName("org.postgresql.Driver");
            @Cleanup Connection connection = DriverManager.getConnection(configProperties.dbUrl(),
                    configProperties.dbUser(), configProperties.dbPassword());
            @Cleanup PreparedStatement preparedStatement = connection.prepareStatement(FIND_BY_EMAIL_PASSWORD);
            preparedStatement.setString(1, email);
            preparedStatement.setString(2, password);
            ResultSet resultSet = preparedStatement.executeQuery();
            List<Developer> list = constructDeveloper(resultSet);
            for (Developer developer : list) {
                return Optional.ofNullable(developer);
            }
        } catch (SQLException | ClassNotFoundException e) {
            logger.warn("couldn't execute find by id query." + e.getMessage());
        }
        return Optional.empty();
    }
}
