package org.employ.repository;

import java.sql.SQLException;
import java.util.List;
import java.util.Optional;

public interface BaseRepository<T> {
    Optional<T> findById(Long id);

    void deleteById(Long id) throws SQLException, ClassNotFoundException;

    List<T> getAll();

    void add(T object) throws SQLException;
}
