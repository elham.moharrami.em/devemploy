package org.employ.repository;

import org.employ.model.Project;

import java.sql.SQLException;

public interface ProjectRepository extends BaseRepository<Project> {
    void deleteByDeveloperId(Long id) throws SQLException;
    void deleteByEmployerId(Long id) throws SQLException;
}
