package org.employ.repository;

import org.employ.model.Developer;

import java.util.Optional;

public interface DeveloperRepository extends BaseRepository<Developer> {
    Optional<Developer> login(String email, String password);
}
