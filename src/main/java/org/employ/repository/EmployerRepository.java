package org.employ.repository;

import org.employ.model.Employer;

import java.util.Optional;

public interface EmployerRepository extends BaseRepository<Employer> {
    Optional<Employer> login(String email, String password);
}
