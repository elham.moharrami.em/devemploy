package org.employ.database.config;

import lombok.Cleanup;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.employ.common.ConfigProperties;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.Statement;

public class DatabaseCreator {
    private final ConfigProperties configProperties = new ConfigProperties();
    private static final Logger logger = LogManager.getLogger(DatabaseCreator.class);

    public void createDatabase() {
        String dbName = configProperties.dbName();
        boolean exists = checkDatabaseExist();
        try {
            @Cleanup Connection connection = DriverManager.getConnection(configProperties.rawDbUrl(),
                    configProperties.dbUser(), configProperties.dbPassword());
            @Cleanup Statement statement = connection.createStatement();
            if (!exists) {
                String createDatabaseQuery = "CREATE DATABASE " + dbName;
                statement.executeUpdate(createDatabaseQuery);
                logger.info("Database " + dbName + " created successfully.");
            } else {
                logger.info("database already exists.");
            }
        } catch (Exception e) {
            logger.error("couldn't create database." + e.getMessage());
        }
    }

    private boolean checkDatabaseExist() {
        try {
            @Cleanup Connection connection = DriverManager.getConnection(configProperties.dbUrl(),
                    configProperties.dbUser(), configProperties.dbPassword());
            return true;
        } catch (Exception e) {
            return false;
        }
    }
}
